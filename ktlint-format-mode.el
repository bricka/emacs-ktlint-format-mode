;;; ktlint-format-mode.el --- Run ktlint when saving a Kotlin buffer -*- lexical-binding: t; -*-

;; Copyright 2023 Alex Figl-Brick

;; Author: Alex Figl-Brick <alex@alexbrick.me>
;; Version: 0.1
;; Package-Requires: ((emacs "25.1"))
;; URL: https://gitlab.com/bricka/emacs-ktlint-format-mode

;;; Commentary:

;; When editing Kotlin code, run ktlint when saving the buffer.

;;; Code:

(defun ktlint-format-mode--run ()
  "Run ktlint to format this buffer."
  (save-mark-and-excursion
    (let* ((src-buffer (current-buffer)))
           (with-temp-buffer
             (let ((out-buffer (current-buffer)))
               (set-buffer src-buffer)
               (call-process-region nil nil "ktlint" nil (list out-buffer nil) nil "--stdin" "--format" "-l" "none")
               (replace-buffer-contents out-buffer))))))

(define-minor-mode ktlint-format-mode
  "A minor mode for Kotlin files that runs ktlint on save to format."
  :lighter nil
  (if ktlint-format-mode
      (when (executable-find "ktlint")
        (add-hook 'before-save-hook #'ktlint-format-mode--run nil t))
    (remove-hook 'before-save-hook #'ktlint-format-mode--run t)))

(provide 'ktlint-format-mode)
;;; ktlint-format-mode.el ends here
